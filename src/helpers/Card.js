import {STORE} from '../config';

export const cardApi = {

    prepareProductToCard(product){
        const {amount, ...prod} = product;
        return JSON.stringify(prod);
    },

    /**
     * Write all items to store
     * @param {*} store 
     */
    writeToStorage(store){
        localStorage.setItem(STORE, JSON.stringify([...store]))
    },

    readFormStorage(){
        return new Map(JSON.parse(localStorage.getItem(STORE)))
    },

    /**
     * Write single elemetn to store
     * @param {*} store 
     * @param {*} product 
     */
    setStore(store, product){
        const key = this.prepareProductToCard(product);
        console.log(store);
         store.has(key) ?  store.set(key, store.get(key) + 1) : store.set(key, 1);

        this.writeToStorage(store)
    },

    TotalCard : (args, elem, defaultVal='0') => {
        elem.innerHTML = args.length ? args.map(el=>el[1]).reduce((a,b)=>a+b) : defaultVal;
    },
    TotalSum : (args, elem, defaultVal='0') =>{
       elem.innerHTML = args.length ? args.map(el=>JSON.parse(el[0]).price * el[1]).reduce((a,b)=>a+b).toFixed(2) : defaultVal;
    },

    removeStore(store, product){
        const key = this.prepareProductToCard(product);
        store.has(key) ?  store.set(key, store.get(key) - 1) : store.delete(key, 1);
       if( store.get(key) == 0){
            store.delete(key, 1)
        }
        this.writeToStorage(store)
    },
};

