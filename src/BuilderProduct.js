import {Builder} from './Builder'
import {cardApi} from './helpers/Card'
import {badgesTotalCard, badgesTotalSumCard, cardContainer} from './NavBar';
import {STORE} from './config';
import {CardBuilder} from "./CardBuilder";
import {BuilderLayouts} from './BuilderLayouts';
const layout = new BuilderLayouts({});

export class BuilderProduct extends Builder{

    constructor(product){
        super();
        this.product = product;

        this.col = 'col-12 col-sm-6 col-md-4 col-lg-3 my-2';
    }

    createSingleCart(){
        const h5 = this.createNewElement('h5',this.product.name,'card-title');
        const p = this.createNewElement('p',this.product.description,'card-text');
        const pAmount = this.createNewElement('p',`Amount: ${this.product.amount}`,'card-text');
        const priceHtml = this.createNewElement('p',`$${this.product.price}`,'card-text');
        const a = this.createNewElement('a','Buy','btn btn-primary',[{name:'href',value:'#'}]);

        a.addEventListener('click', e => {
            e.preventDefault();
            cardContainer.innerHTML = '';
            cardApi.setStore(cardApi.readFormStorage(), this.product);
            const cardStore = JSON.parse(localStorage.getItem(STORE));
            cardApi.TotalCard(cardStore, badgesTotalCard);
            cardApi.TotalSum(cardStore, badgesTotalSumCard);
            const cardStoreHtml = cardStore.map(el=>{
                return new CardBuilder(JSON.parse(el[0])).createSingleCart(el[1]);
            });
            layout.render('card',layout.createCardContainer(cardStoreHtml));
        });

        const cardBody = this.createNewElement('div',[h5,p,pAmount,priceHtml,a],'card-body');

        const img = this.createNewElement('img',null,'card-img-top',[{name:'src',value:`./img/${this.product.categories}.webp`}]);

        const card = this.createNewElement('div',[img,cardBody],'card');
        return this.createNewElement('div',[card],this.col);
    }
}
