import {Builder} from  './Builder';

const builder = new Builder;


const textTotalSum  = builder.createNewElement('span', ' Cad Total Sum: ');
export const badgesTotalSumCard = builder.createNewElement('span', null, 'badge badge-primary');
export const badgesTotalCard  = builder.createNewElement('span', null, 'badge badge-primary');
const textCard = builder.createNewElement('span', 'Card ');

const totalInfoCard = builder.createNewElement('div', [textTotalSum,badgesTotalSumCard], 'total-info-card');

const homeLink  = builder.createNewElement('a', 'Home', 'nav-link',[{name: 'href', value: '#'}]);
const cardLink  = builder.createNewElement('a', [textCard, badgesTotalCard], 'nav-link',[{name: 'href', value: '#'}]);

export const cardContainer =  builder.createNewElement('div', null, 'card-nav',[{name: 'id', value: 'card'}]);
const containerCard =  builder.createNewElement('div', [cardContainer, totalInfoCard], 'card-container-nav invisible');

const liItem1 = builder.createNewElement('li', [homeLink], 'nav-item');
const liItem2 = builder.createNewElement('li', [cardLink, containerCard ], 'nav-item', [{name: 'id', value: 'cardNav'}]);

cardLink.addEventListener('click', e => {
        e.preventDefault();
        containerCard.classList.toggle('invisible');
});

const ul  = builder.createNewElement('ul', [liItem1, liItem2], 'navbar-nav w-100 justify-content-end');

const collapse  = builder.createNewElement('div', [ul], 'collapse navbar-collapse', [{name: "id", value: "navbarSupportedContent"}]);

const navbarToggleIcon  = builder.createNewElement('span', null, 'navbar-toggler-icon');
const button = builder.createNewElement('button', [navbarToggleIcon], 'navbar-toggler',
    [
        {name:'data-toggle',value:'collapse'},
        {name:'data-target',value:'#navbarSupportedContent'},
        {name:'aria-controls',value:'navbarSupportedContent'},
        {name:'aria-expanded',value:'false'},
        {name:'aria-label',value:'Toggle navigation'},
    ]);

const brandLink = builder.createNewElement('a', 'Logo', 'navbar-brand',[{name: 'href', value: '#'}]);


export const nav = builder.createNewElement('nav', [brandLink, button, collapse], 'navbar navbar-expand-lg navbar-light bg-light');