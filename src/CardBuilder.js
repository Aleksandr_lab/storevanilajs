import {Builder} from './Builder'
import {cardApi} from './helpers/Card'
import {STORE} from './config';
import {badgesTotalCard, badgesTotalSumCard, cardContainer} from "./NavBar";
import {BuilderLayouts} from "./BuilderLayouts";
const layout = new BuilderLayouts({});


export class CardBuilder extends Builder{

    constructor(product){
        super();
        this.product = product;

        this.col = 'col-12';
    }

    createSingleCart(amount){
        const h6 = this.createNewElement('h6',this.product.name,'card-title');
        const br = this.createNewElement('br');
        const priceHtml = this.createNewElement('span',`$${(this.product.price * amount).toFixed(2)}`,'card-price');
        const textPrice = this.createNewElement('span','Total price product: ','card-price-text');
        const cardAmount = this.createNewElement('span',amount,'card-total');
        const textAmount = this.createNewElement('span','Total amount product: ','card-amount-text');
        const infoCard = this.createNewElement('div',[textPrice, priceHtml,br, textAmount, cardAmount],'card-info');
        const deleteProduct = this.createNewElement('button', 'delete', 'btn-delete-product', [{name: 'data-id-product', value: this.product.id}]);
        const cardBody = this.createNewElement('div',[h6, deleteProduct, infoCard],'col-8 p-1 product-bodu-card');
        deleteProduct.addEventListener('click', ()=>{
            cardContainer.innerHTML = '';
            cardApi.removeStore(cardApi.readFormStorage(),this.product);
            const cardStore = JSON.parse(localStorage.getItem(STORE));
            cardApi.TotalCard(cardStore, badgesTotalCard);
            cardApi.TotalSum(cardStore, badgesTotalSumCard);

            const cardStoreHtml = cardStore.map(el=>{
                 return new CardBuilder(JSON.parse(el[0])).createSingleCart(el[1]);
            });
            console.log(cardStoreHtml)
            layout.render('card',layout.createCardContainer(cardStoreHtml));
        });
        const img = this.createNewElement('img',null,'',[{name:'src',value:`./img/${this.product.categories}.webp`}]);
        const imagebox = this.createNewElement('div', [img],'col-4 p-1' );

        const card = this.createNewElement('div',[imagebox,cardBody],'row');
        return this.createNewElement('div',[card],this.col);
    }
}